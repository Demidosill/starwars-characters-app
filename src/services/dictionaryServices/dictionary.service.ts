import { IFiltersParams } from './dictionary.model';

export const getGender = async (): Promise<IFiltersParams[]> => {
    const response = await fetch('http://localhost:5000/api/STAR_WARS/gender');
    const data: IFiltersParams[] = await response.json();
    return data;
};

export const getRace = async (): Promise<IFiltersParams[]> => {
    const response = await fetch('http://localhost:5000/api/STAR_WARS/race');
    const data: IFiltersParams[] = await response.json();
    return data;
};

export const getSide = async (): Promise<IFiltersParams[]> => {
    const response = await fetch('http://localhost:5000/api/STAR_WARS/side');
    const data: IFiltersParams[] = await response.json();
    return data;
};
