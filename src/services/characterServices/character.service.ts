import { ICharacter } from '../../core/domain/character/character.type';

import { characterUrl } from './character.constants';
import { viewedCharacterFactory, charactersDataFactory } from './character.factory';
import { ICharactersModel, IPostCharactersContent } from './character.model';

export const getData = async (url: string): Promise<ICharacter[]> => {
    const response = await fetch(url);

    if (!response.ok) {
        throw new Error(`Ошибка по адресу ${url}, статус ошибки ${response.status}`);
    }

    const data: ICharactersModel = await response.json();
    return charactersDataFactory(data.content);
};

export const getCard = async (queryID: string): Promise<ICharacter> => {
    const response = await fetch(`${characterUrl}${queryID}`);

    if (!response.ok) {
        throw new Error(`Ошибка по адресу ${characterUrl}${queryID}, статус ошибки ${response.status}`);
    }

    return viewedCharacterFactory(await response.json());
};

export const postNewCharacter = async (url: string, data: IPostCharactersContent): Promise<void> => {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify(data),
    });

    if (!response.ok) {
        throw new Error(`Ошибка по адресу ${url}, статус ошибки ${response.status}`);
    }
};
