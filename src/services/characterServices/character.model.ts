import { IFiltersParams } from '../dictionaryServices/dictionary.model';

export interface ICharacters {
    id: string;
    name: string;
    imageURL: string;
    nameColor: string;
    backgroundColor: string;
    parametersColor: string;
}

export interface ICharactersModel {
    content: ICharactersContent[];
}

export interface ICharactersContentView extends ICharacters {
    description: string;
    gender: IFiltersParams;
    race: IFiltersParams;
    side: IFiltersParams;
    tag1: string;
    tag2: string;
    tag3: string;
}

export interface IPostCharactersContent {
    id?: string;
    name: string;
    imageURL: string;
    nameColor: string;
    backgroundColor: string;
    parametersColor: string;
    description: string;
    gender: IFiltersParams;
    race: IFiltersParams;
    side: IFiltersParams;
    tag1: string;
    tag2: string;
    tag3: string;
}

export interface ICharactersContent extends ICharacters {
    gender: string;
    race: string;
    side: string;
}
