import { ICharacter } from './../../core/domain/character/character.type';
import { ICharactersContent, ICharactersContentView } from './character.model';

export const charactersDataFactory = (characterModel: ICharactersContent[]): ICharacter[] => {
    return characterModel.map((item) => ({
        id: item.id,
        name: item.name,
        image: item.imageURL,
        nameColor: item.nameColor,
        parametersBgColor: item.backgroundColor,
        parametersColor: item.parametersColor,
        tags: [],
        gender: item.gender,
        race: item.race,
        side: item.side,
    }));
};

export const viewedCharacterFactory = (characterModel: ICharactersContentView): ICharacter => {
    return {
        id: characterModel.id,
        name: characterModel.name,
        image: characterModel.imageURL,
        nameColor: characterModel.nameColor,
        parametersBgColor: characterModel.backgroundColor,
        parametersColor: characterModel.parametersColor,
        descriptions: characterModel.description,
        tags: [characterModel.tag1, characterModel.tag2, characterModel.tag3],
        gender: characterModel.gender.value,
        race: characterModel.race.value,
        side: characterModel.side.value,
    };
};
