import { FC } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import CharactersPage from '../pages/characters/CharactersPage';
import HomePage from '../pages/home/HomePage';
import { charactersPageUrl } from '../services/characterServices/character.constants';

const Routing: FC = () => {
    return (
        <Switch>
            <Route exact path="/">
                <HomePage />
            </Route>
            <Route exact path={`${charactersPageUrl}/new`}>
                <CharactersPage />
            </Route>
            <Route exact path={`${charactersPageUrl}/:id`}>
                <CharactersPage />
            </Route>
            <Route exact path={charactersPageUrl}>
                <CharactersPage />
            </Route>
            <Redirect to="/" />
        </Switch>
    );
};

export default Routing;
