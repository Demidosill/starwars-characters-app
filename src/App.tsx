import { FC } from 'react';

import { IMenuLink } from './components/link/Link';
import Header from './core/components/header/Header';
import Routing from './route/Router';
import { charactersPageUrl } from './services/characterServices/character.constants';

const menuLink: IMenuLink[] = [
    { id: 1, name: 'Home', url: '/' },
    { id: 2, name: 'Characters', url: charactersPageUrl },
];

const App: FC = () => {
    return (
        <>
            <Header links={menuLink} />
            <Routing />
        </>
    );
};

export default App;
