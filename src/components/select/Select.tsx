import { FC, useEffect, useState } from 'react';

import SelectorBody from './SelectorBody';
import { ISelector } from './Selectors';

interface ISelectProps {
    settings: ISelector;
    key: number;
}

const Select: FC<ISelectProps> = (props) => {
    const [isOpen, setOpen] = useState<boolean>(false);
    const [icon, setIcon] = useState<string>(props.settings.inActiveIcon);
    const [headerClass, setHeaderClass] = useState<string>('select__header');
    const [activeOptions, setActiveOptions] = useState<string[]>([]);
    const [selectorTitle, setSelectorTitle] = useState<string>(props.settings.title);

    const showOptionTextInHeader = (newLabel: string): void => {
        if (activeOptions.length === 0) {
            setActiveOptions([...activeOptions, newLabel]);
        } else {
            for (let i = 0; i < activeOptions.length; i++) {
                if (activeOptions[i] === newLabel) {
                    setActiveOptions(activeOptions.filter((item) => item !== newLabel));
                    break;
                } else {
                    setActiveOptions([...activeOptions, newLabel]);
                }
            }
        }
    };

    useEffect(() => {
        if (activeOptions.length === 0) {
            setSelectorTitle(props.settings.title);
        } else if (activeOptions.length === 1) {
            setSelectorTitle(activeOptions[0]);
        } else {
            setSelectorTitle(`Selected: ${activeOptions.length}`);
        }
    }, [activeOptions]);

    const showSelectorOptions = (state: boolean): void => {
        setOpen(state);

        if (state) {
            setIcon(props.settings.activeIcon);
            setHeaderClass('select__header active');
        } else {
            setIcon(props.settings.inActiveIcon);
            setHeaderClass('select__header');
        }
    };

    return (
        <div className="select">
            <div className={headerClass} onClick={(): void => showSelectorOptions(!isOpen)}>
                <div className="select__title" data-title={props.settings.title}>
                    {selectorTitle}
                </div>
                <div className="select__icon">
                    <img className="select__icon-img" src={icon} alt="select arrow down"></img>
                </div>
            </div>
            <SelectorBody
                isOpen={isOpen}
                options={props.settings.options}
                title={props.settings.title}
                showOptionTextInHeader={showOptionTextInHeader}
            />
        </div>
    );
};

export default Select;
