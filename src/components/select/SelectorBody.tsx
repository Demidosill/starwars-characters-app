import { FC } from 'react';

import { useAppSelector } from '../../redux/store.hooks';

import { IFiltersParams } from '../../services/dictionaryServices/dictionary.model';

import SelectOption from './SelectOption';

interface ISelectorBodyProps {
    isOpen: boolean;
    options: IFiltersParams[];
    title: string;
    showOptionTextInHeader: (newLabel: string) => void;
}

const SelectorBody: FC<ISelectorBodyProps> = (props) => {
    const filterData = useAppSelector((state) => state.characterPageState.filterData);

    const isChecked = (id: string): boolean => {
        let toggle: boolean = false;

        for (const key in filterData) {
            if (key === props.title.toLocaleLowerCase()) {
                const filtersArray: string[] = filterData[key] as string[];

                for (let i = 0; i < filtersArray.length; i++) {
                    if (filtersArray[i] === id) {
                        toggle = true;
                    }
                }
            }
        }

        return toggle;
    };

    const classCreator = (): string => {
        if (props.isOpen) {
            return 'select__body active';
        } else {
            return 'select__body';
        }
    };

    return (
        <ul className={classCreator()}>
            {props.options.map((option) => (
                <SelectOption
                    isChecked={isChecked(option.id)}
                    key={option.id}
                    option={option}
                    title={props.title}
                    showOptionTextInHeader={props.showOptionTextInHeader}
                />
            ))}
        </ul>
    );
};

export default SelectorBody;
