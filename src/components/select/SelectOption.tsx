import { ChangeEvent, FC } from 'react';

import { setFilterData } from '../../redux/character/characterPage.slice';

import { useAppDispatch, useAppSelector } from '../../redux/store.hooks';
import { IFiltersParams } from '../../services/dictionaryServices/dictionary.model';

interface ISelectOptionProps {
    isChecked: boolean;
    option: IFiltersParams;
    title: string;
    showOptionTextInHeader: (newLabel: string) => void;
}

const SelectOption: FC<ISelectOptionProps> = (props) => {
    const filtersData = useAppSelector((state) => state.characterPageState.filterData);
    const dispatch = useAppDispatch();

    const updateTypesArray = (typesArray: string[], target: EventTarget & HTMLInputElement): string[] => {
        if (target.checked) {
            return [...typesArray, props.option.id];
        } else {
            return typesArray.filter((id) => id !== props.option.id);
        }
    };

    const updateFiltersData = (event: ChangeEvent<HTMLInputElement>): void => {
        for (const key in filtersData) {
            if (key === props.title.toLocaleLowerCase()) {
                dispatch(
                    setFilterData({
                        ...filtersData,
                        [props.title.toLocaleLowerCase()]: updateTypesArray(filtersData[key], event.target),
                    })
                );
            }
        }
    };

    return (
        <li className="select__item" onChange={(): void => props.showOptionTextInHeader(props.option.value)}>
            <label className="select__label">
                <input
                    checked={props.isChecked}
                    className="select__checkbox"
                    type="checkbox"
                    value={props.option.value}
                    onChange={updateFiltersData}
                ></input>
                <span className="checkbox-btn"></span>
                {props.option.value}
            </label>
        </li>
    );
};

export default SelectOption;
