import { FC } from 'react';

import inActiveArrowDown from '../../assets/image/selectArrowDown.png';
import activeArrowDown from '../../assets/image/selectArrowDownActive.png';
import { useAppSelector } from '../../redux/store.hooks';
import { IFiltersParams } from '../../services/dictionaryServices/dictionary.model';

import Select from './Select';

export interface ISelector {
    id: number;
    title: string;
    options: IFiltersParams[];
    inActiveIcon: string;
    activeIcon: string;
}

const Selectors: FC = () => {
    const characterPage = useAppSelector((state) => state.characterPageState);
    const selectorArray: ISelector[] = [
        {
            id: 1,
            title: 'Gender',
            options: characterPage.genderFilters,
            inActiveIcon: inActiveArrowDown,
            activeIcon: activeArrowDown,
        },
        {
            id: 2,
            title: 'Race',
            options: characterPage.raceFilters,
            inActiveIcon: inActiveArrowDown,
            activeIcon: activeArrowDown,
        },
        {
            id: 3,
            title: 'Side',
            options: characterPage.sideFilters,
            inActiveIcon: inActiveArrowDown,
            activeIcon: activeArrowDown,
        },
    ];
    return (
        <div className="main__selectors">
            {selectorArray.map((select) => (
                <Select key={select.id} settings={select} />
            ))}
        </div>
    );
};

export default Selectors;
