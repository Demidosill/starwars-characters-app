import { FC } from 'react';

interface IButtonCloseProps {
    img: string;
    closeHandler: () => void;
}

const ButtonClose: FC<IButtonCloseProps> = (props) => {
    return (
        <button className="preview__close-btn" onClick={props.closeHandler}>
            <img src={props.img} alt="Close" />
        </button>
    );
};

export default ButtonClose;
