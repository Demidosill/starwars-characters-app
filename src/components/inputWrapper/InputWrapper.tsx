import { FC } from 'react';

interface IInputWrapper {
    classes: string;
    label: string;
    help?: string;
    name: string;
}

const InputWrapper: FC<IInputWrapper> = (props) => {
    return (
        <div className={props.classes}>
            <label className="info__label" htmlFor={props.name} title={props.help}>
                {props.label}
            </label>
            {props.children}
        </div>
    );
};

export default InputWrapper;
