import { FC } from 'react';
import { NavLink } from 'react-router-dom';

export interface IMenuLink {
    id: number;
    name: string;
    url: string;
}

interface ILinkProps {
    link: IMenuLink;
    key: number;
}

export const Link: FC<ILinkProps> = (props) => {
    return (
        <li className="list__item">
            <NavLink className="list__item-link" to={props.link.url}>
                {props.link.name}
            </NavLink>
        </li>
    );
};
