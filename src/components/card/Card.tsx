import CSS from 'csstype';
import { FC } from 'react';
import { useHistory } from 'react-router-dom';

import { ICharacter } from '../../core/domain/character/character.type';
import { setViewCharacterModalToggle } from '../../redux/character/characterPage.slice';
import { useAppDispatch } from '../../redux/store.hooks';
import { charactersPageUrl } from '../../services/characterServices/character.constants';
import { getCard } from '../../services/characterServices/character.service';

interface ICardProps {
    card: ICharacter;
}

const Card: FC<ICardProps> = (props) => {
    const dispatch = useAppDispatch();
    const history = useHistory();

    const nameColor: CSS.Properties = {
        color: props.card.nameColor ? props.card.nameColor : '#ffffff',
    };
    const paramsBgColor: CSS.Properties = {
        backgroundColor: props.card.parametersBgColor ? props.card.parametersBgColor : '#000000',
    };
    const paramsColor: CSS.Properties = {
        color: props.card.parametersColor ? props.card.parametersColor : '#ffffff',
    };
    const setClassName = (): string => {
        if (props.card.side.toLocaleLowerCase() === 'темная') {
            return 'registry__item registry__item_dark item';
        } else if (props.card.side.toLocaleLowerCase() === 'светлая') {
            return 'registry__item registry__item_light item';
        } else {
            return 'registry__item registry__item_undefined item';
        }
    };
    return (
        <div
            className={setClassName()}
            onClick={(): void => {
                const getViewedCard = async (): Promise<void> => {
                    dispatch(
                        setViewCharacterModalToggle({ isOpen: true, viewedCharacter: await getCard(props.card.id) })
                    );
                    history.push(`${charactersPageUrl}/${props.card.id}`);
                };

                void getViewedCard();
            }}
        >
            <div className="item__head">
                <span className="item__name" style={nameColor}>
                    {props.card.name ? props.card.name : 'Unknown'}
                </span>
                {props.card.image ? (
                    <img className="item__photo" src={props.card.image} alt={props.card.name}></img>
                ) : null}
            </div>
            <div className="item__description" style={paramsBgColor}>
                <div className="item__gender description__text" style={paramsColor}>
                    <span>Gender</span>
                    <span>{props.card.gender ? props.card.gender : 'Unknown'}</span>
                </div>
                <div className="item__race description__text" style={paramsColor}>
                    <span>Race</span>
                    <span>{props.card.race ? props.card.race : 'Unknown'}</span>
                </div>
                <div className="item__side description__text" style={paramsColor}>
                    <span>Side</span>
                    <span>{props.card.side ? props.card.side : 'Unknown'}</span>
                </div>
            </div>
        </div>
    );
};

export default Card;
