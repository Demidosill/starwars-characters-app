import { FC, HTMLAttributes } from 'react';

export interface IInput extends HTMLAttributes<HTMLInputElement> {
    id?: string;
    name: string;
    classes: string[];
    type: string;
    placeholder?: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: FC<IInput> = (props) => {
    const { id, placeholder, name, type, classes, ...rest } = props;
    return (
        <input
            {...rest}
            className={classes.join(' ')}
            type={type}
            id={name}
            name={name}
            placeholder={placeholder}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
                props.onChange(event);
            }}
        ></input>
    );
};

export default Input;
