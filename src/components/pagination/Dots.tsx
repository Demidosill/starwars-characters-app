import { FC } from 'react';

interface IDotsProps {
    dotIndex: number;
    currentIndex: number;
    getActualIndex: (newIndex: number, direction: string) => void;
}

const Dots: FC<IDotsProps> = (props) => {
    const addDots = (): string => {
        if (props.dotIndex === props.currentIndex) {
            return 'dot active-dot';
        } else {
            return 'dot';
        }
    };

    return <li className={addDots()} onClick={(): void => props.getActualIndex(props.dotIndex, 'next')}></li>;
};

export default Dots;
