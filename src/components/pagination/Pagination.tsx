import { FC } from 'react';

import arrowLeft from '../../assets/image/arrowLeft.png';
import arrowRight from '../../assets/image/arrowRight.png';

import Dots from './Dots';

interface IPaginationProps {
    currentIndex: number;
    totalDotsNumber: number;
    getActualIndex: (newIndex: number, direction: string) => void;
}

const Pagination: FC<IPaginationProps> = (props) => {
    const tempArray: number[] = [];

    for (let i = 0; i < props.totalDotsNumber; i++) {
        tempArray.push(i);
    }

    return (
        <div className="pagination">
            <div className="pagination__container">
                <img
                    className="arrow arrow-left"
                    src={arrowLeft}
                    alt="Left"
                    onClick={(): void => props.getActualIndex(props.currentIndex - 1, 'prevArrow')}
                ></img>
                <div className="dot__wrapper">
                    <ul className="dot__container">
                        {tempArray.map((_, index) => (
                            <Dots
                                key={index}
                                dotIndex={index}
                                currentIndex={props.currentIndex}
                                getActualIndex={props.getActualIndex}
                            />
                        ))}
                    </ul>
                </div>
                <img
                    className="arrow arrow-right"
                    src={arrowRight}
                    alt="Right"
                    onClick={(): void => props.getActualIndex(props.currentIndex + 1, 'nextArrow')}
                ></img>
            </div>
        </div>
    );
};

export default Pagination;
