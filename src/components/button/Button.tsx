import { FC, HTMLAttributes } from 'react';
import { NavLink } from 'react-router-dom';

import { charactersPageUrl } from '../../services/characterServices/character.constants';

interface IButtonProps extends HTMLAttributes<HTMLButtonElement> {
    url: string;
    text: string;
    classes: string[];
}

const Button: FC<IButtonProps> = (props): React.ReactElement<HTMLButtonElement> => {
    const { url, text, classes, ...rest } = props;
    return (
        <NavLink to={charactersPageUrl}>
            <button {...rest} className={classes.join(' ')}>
                {text}
            </button>
        </NavLink>
    );
};

export default Button;
