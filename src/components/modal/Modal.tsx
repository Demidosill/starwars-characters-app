import React, { FC } from 'react';

import './style.css';

interface IModalProps {
    currentModal: React.ReactElement;
    modalType: string;
    closeModal: () => void;
}

const Modal: FC<IModalProps> = (props) => {
    const setClassName = (): string => {
        if (props.modalType === 'addCharacter') {
            return 'modal__wrapper modal__wrapper_centerScaleAnimation';
        } else {
            return 'modal__wrapper modal__wrapper_centerSpinAnimation';
        }
    };
    return (
        <div
            className="modal"
            onClick={(event: React.MouseEvent<HTMLDivElement>): void => {
                if ((event.target as HTMLDivElement).matches('.modal')) {
                    props.closeModal();
                }
            }}
        >
            <div className={setClassName()}>{props.currentModal}</div>
        </div>
    );
};

export default Modal;
