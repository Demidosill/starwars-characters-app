import { FC, useState } from 'react';

import { ICharacter } from '../../core/domain/character/character.type';

import { useAppSelector } from '../../redux/store.hooks';
import Card from '../card/Card';

import Pagination from '../pagination/Pagination';

const Registry: FC = () => {
    const characterData = useAppSelector((state) => state.characterPageState.characterData);
    const [index, setIndex] = useState(0);
    const NUMBER_OF_CARDS_PER_PAGE: number = 3;
    const MAX_CARDS_NUMBER: number = 15;
    const MAX_DOTS_NUMBER: number = 5;
    const TOTAL_DOTS_NUMBER: number = characterData.length > MAX_CARDS_NUMBER ? MAX_DOTS_NUMBER : characterData.length / NUMBER_OF_CARDS_PER_PAGE;
    const cardsList: ICharacter[] = [];
    const START = index * NUMBER_OF_CARDS_PER_PAGE;
    const END = index * NUMBER_OF_CARDS_PER_PAGE + NUMBER_OF_CARDS_PER_PAGE;

    if (END <= MAX_CARDS_NUMBER) {
        for (let i = START; i < END; i++) {
            if (characterData[i]) {
                cardsList.push(characterData[i]);
            } else {
                break;
            }
        }
    }

    const getActualIndex = (newIndex: number): void => {
        if (newIndex === TOTAL_DOTS_NUMBER || newIndex < 0) {
            return;
        } else {
            setIndex(newIndex);
        }
    };

    return (
        <div className="main__registry registry">
            <div className="registry__wrapper">
                {cardsList.map((character) => (
                    <Card key={character.id} card={character} />
                ))}
            </div>
            <Pagination currentIndex={index} totalDotsNumber={TOTAL_DOTS_NUMBER} getActualIndex={getActualIndex} />
        </div>
    );
};

export default Registry;
