import { createSlice } from '@reduxjs/toolkit';

import { ICharacter } from '../../core/domain/character/character.type';
import { IFiltersParams } from '../../services/dictionaryServices/dictionary.model';

import { ICharacterPageState, ICharacterPageAction, IFilterData } from './characterPage.type';

const initialState: ICharacterPageState = {
    characterData: [],
    filterData: {
        values: '',
        gender: [],
        race: [],
        side: [],
    },
    genderFilters: [],
    raceFilters: [],
    sideFilters: [],
    addCharacterModalToggle: false,
    viewCharacterModalToggle: false,
    viewedCharacter: null,
};

const charactersSlice = createSlice({
    name: 'characters',
    initialState: initialState,
    reducers: {
        setCharacterData: (state, action: ICharacterPageAction<ICharacter[]>) => {
            state.characterData = action.payload;
        },
        setFilterData: (state, action: ICharacterPageAction<IFilterData>) => {
            state.filterData = action.payload;
        },
        setGenderFilters: (state, action: ICharacterPageAction<IFiltersParams[]>) => {
            state.genderFilters = action.payload;
        },
        setRaceFilters: (state, action: ICharacterPageAction<IFiltersParams[]>) => {
            state.raceFilters = action.payload;
        },
        setSideFilters: (state, action: ICharacterPageAction<IFiltersParams[]>) => {
            state.sideFilters = action.payload;
        },
        setAddCharacterModalToggle: (state, action: ICharacterPageAction<boolean>) => {
            state.addCharacterModalToggle = action.payload;
        },
        setViewCharacterModalToggle: (state, action: ICharacterPageAction<{ isOpen: boolean; viewedCharacter: ICharacter | null }>) => {
            state.viewCharacterModalToggle = action.payload.isOpen;
            state.viewedCharacter = action.payload.viewedCharacter;
        },
    },
});

export const {
    setCharacterData,
    setFilterData,
    setGenderFilters,
    setRaceFilters,
    setSideFilters,
    setAddCharacterModalToggle,
    setViewCharacterModalToggle,
} = charactersSlice.actions;
export default charactersSlice.reducer;
