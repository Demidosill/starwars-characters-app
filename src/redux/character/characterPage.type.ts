import { ICharacter } from '../../core/domain/character/character.type';
import { IFiltersParams } from '../../services/dictionaryServices/dictionary.model';

export interface IFilterData {
    values: string;
    gender: string[];
    race: string[];
    side: string[];
}

export interface ICharacterPageState {
    characterData: ICharacter[];
    filterData: IFilterData;
    genderFilters: IFiltersParams[];
    raceFilters: IFiltersParams[];
    sideFilters: IFiltersParams[];
    addCharacterModalToggle: boolean;
    viewCharacterModalToggle: boolean;
    viewedCharacter: null | ICharacter;
}

export interface ICharacterPageAction<Data> {
    type: string;
    payload: Data;
}
