import { configureStore, combineReducers } from '@reduxjs/toolkit';

import characterPageReducer from './character/characterPage.slice';

const rootReducer = combineReducers({
    characterPageState: characterPageReducer,
});

export const store = configureStore({
    reducer: rootReducer,
});

export type RootStateType = ReturnType<typeof rootReducer>;
export type RootStoreType = typeof store;
export type AppStateType = ReturnType<RootStoreType['getState']>;
export type AppDispatchType = RootStoreType['dispatch'];
