import { FC } from 'react';

import logo from '../../../assets/image/logo.png';
import { Link, IMenuLink } from '../../../components/link/Link';

interface IHeaderProps {
    links: IMenuLink[];
}

const Header: FC<IHeaderProps> = (props) => {
    return (
        <header className="header">
            <div className="header__logo logo">
                <img className="logo__img" src={logo} alt="Star Wars logo"></img>
            </div>
            <nav className="header__nav nav">
                <ul className="nav__list list">
                    {props.links.map((link) => (
                        <Link key={link.id} link={link} />
                    ))}
                </ul>
            </nav>
        </header>
    );
};

export default Header;
