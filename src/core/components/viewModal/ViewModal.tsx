import { FC } from 'react';

import close from '../../../assets/image/close.png';

import ButtonClose from '../../../components/buttonClose/ButtonClose';

import './style.css';
import { ICharacter } from '../../domain/character/character.type';

export interface ViewModalProps {
    closeModal: () => void;
    fullSize: boolean;
    viewedCharacter: ICharacter | null;
}

const ViewModal: FC<ViewModalProps> = (props) => {
    const setClassName = (defaultClass: string, fakeClass: string): string => {
        if (props.fullSize) {
            return defaultClass;
        } else {
            return fakeClass;
        }
    };
    const showTags = (): string => {
        let className: string = '';

        if (props.viewedCharacter?.tags) {
            if (props.viewedCharacter?.tags[0] === null) {
                className = 'view__tags-item';
            } else {
                className = 'view__tags-item active';
            }
        }

        return className;
    };

    return (
        <div className={setClassName('view__wrapper', 'fakeView__wrapper')}>
            <div className={setClassName('view__info', 'fakeView__info')}>
                <div className={setClassName('view__title', 'fakeView__title')}>
                    <span>{props.viewedCharacter?.name ? props.viewedCharacter?.name : 'Unknown'}</span>
                </div>
                <div className={setClassName('view__characteristic', 'fakeView__characteristic')}>
                    <div className={setClassName('view__characteristic-item', 'fakeView__characteristic-item')}>
                        <span className="view__characteristic-title">Gender</span>
                        <span className="view__characteristic-type">{props.viewedCharacter?.gender ? props.viewedCharacter?.gender : 'Unknown'}</span>
                    </div>
                    <div className={setClassName('view__characteristic-item', 'fakeView__characteristic-item')}>
                        <span className="view__characteristic-title">Race</span>
                        <span className="view__characteristic-type">{props.viewedCharacter?.race ? props.viewedCharacter?.race : 'Unknown'}</span>
                    </div>
                    <div className={setClassName('view__characteristic-item', 'fakeView__characteristic-item')}>
                        <span className="view__characteristic-title">Side</span>
                        <span className="view__characteristic-type">{props.viewedCharacter?.side ? props.viewedCharacter?.side : 'Unknown'}</span>
                    </div>
                </div>
                <div className={setClassName('view__descr', 'fakeView__descr')}>
                    <p className={setClassName('view__descr-text', 'fakeView__descr-text')}>{props.viewedCharacter?.descriptions}</p>
                </div>
            </div>
            <div className="right">
                {props.fullSize ? (
                    <div className="view__close">
                        <ButtonClose img={close} closeHandler={props.closeModal} />
                    </div>
                ) : null}
                <div className="view__tags">
                    <div className={`view__tags-1 ${showTags()}`}>{props.viewedCharacter?.tags[0]}</div>
                    <div className={`view__tags-2 ${showTags()}`}>{props.viewedCharacter?.tags[1]}</div>
                    <div className={`view__tags-3 ${showTags()}`}>{props.viewedCharacter?.tags[2]}</div>
                </div>
            </div>
            {props.viewedCharacter?.image ? <img className="view__bg" src={props.viewedCharacter?.image} alt="bg" /> : null}
        </div>
    );
};

export default ViewModal;
