import { Field, Form, Formik } from 'formik';
import React, { FC, useState } from 'react';

import * as yup from 'yup';

import close from '../../../assets/image/close.png';
import ButtonClose from '../../../components/buttonClose/ButtonClose';
import Card from '../../../components/card/Card';
import InputWrapper from '../../../components/inputWrapper/InputWrapper';
import Pagination from '../../../components/pagination/Pagination';
import { useAppSelector } from '../../../redux/store.hooks';

import { viewedCharacterFactory } from '../../../services/characterServices/character.factory';
import { ICharactersContentView, IPostCharactersContent } from '../../../services/characterServices/character.model';
import { postNewCharacter } from '../../../services/characterServices/character.service';
import { IFiltersParams } from '../../../services/dictionaryServices/dictionary.model';
import ViewModal from '../viewModal/ViewModal';

import './style.css';

interface AddModalProps {
    closeModal: () => void;
}

const AddModal: FC<AddModalProps> = (props) => {
    const [index, setIndex] = useState(0);
    const TOTAL_DOTS_NUMBER: number = 2;
    const MAX_TAGS_NUMBER: number = 3;
    const ganderFilters = useAppSelector((state) => state.characterPageState.genderFilters);
    const raceFilters = useAppSelector((state) => state.characterPageState.raceFilters);
    const sideFilters = useAppSelector((state) => state.characterPageState.sideFilters);
    const newCharacter: ICharactersContentView = {
        id: '',
        name: '',
        gender: { id: '', value: '' },
        race: { id: '', value: '' },
        side: { id: '', value: '' },
        imageURL: '',
        description: '',
        tag1: '',
        tag2: '',
        tag3: '',
        nameColor: '#ffffff',
        backgroundColor: '#000000',
        parametersColor: '#ffffff',
    };

    const validationSchema = yup.object().shape({
        name: yup.string().required('Enter name'),
        gender: yup.object().shape({
            value: yup
                .string()
                .required('Enter gender')
                .matches(/Мужчина|Женщина|Неизвестен/),
        }),
        race: yup.object().shape({
            value: yup
                .string()
                .required('Enter race')
                .matches(/Человек|Дроид|Вуки|Неизвестен/),
        }),
        side: yup.object().shape({
            value: yup
                .string()
                .required('Enter side')
                .matches(/Светлая|Темная|Неизвестен/),
        }),
    });
    const resetNewCharacter = (): void => {
        newCharacter.id = '';
        newCharacter.name = '';
        newCharacter.gender = { id: '', value: '' };
        newCharacter.race = { id: '', value: '' };
        newCharacter.side = { id: '', value: '' };
        newCharacter.imageURL = '';
        newCharacter.description = '';
        newCharacter.tag1 = '';
        newCharacter.tag2 = '';
        newCharacter.tag3 = '';
        newCharacter.nameColor = '#ffffff';
        newCharacter.backgroundColor = '#000000';
        newCharacter.parametersColor = '#000000';
    };
    const setFilterID = (filterType: IFiltersParams, title: string): string => {
        if (title === 'gender') {
            filterType.id = ganderFilters.filter((type) => filterType.value === type.value)[0].id;
        } else if (title === 'race') {
            filterType.id = raceFilters.filter((type) => filterType.value === type.value)[0].id;
        } else {
            filterType.id = sideFilters.filter((type) => filterType.value === type.value)[0].id;
        }

        return filterType.id;
    };

    const getActualIndex = (newIndex: number): void => {
        if (newIndex === TOTAL_DOTS_NUMBER || newIndex < 0) {
            return;
        } else {
            setIndex(newIndex);
        }
    };

    return (
        <Formik
            initialValues={newCharacter}
            validateOnBlur
            validationSchema={validationSchema}
            onSubmit={(values): void => {
                values.gender.id = setFilterID(values.gender, 'gender');
                values.race.id = setFilterID(values.race, 'race');
                values.side.id = setFilterID(values.side, 'side');

                const deleteID = (values: IPostCharactersContent): IPostCharactersContent => {
                    delete values.id;
                    return values;
                };

                void postNewCharacter('http://localhost:5000/api/STAR_WARS/character', deleteID(values));
                props.closeModal();
                resetNewCharacter();
            }}
        >
            {({ values, errors, touched, isValid, dirty, setFieldValue }): React.ReactElement => (
                <Form>
                    <div className="add__wrapper">
                        {/************************** ADD CHARACTER INFO **************************/}
                        <div className="info modal__info">
                            <InputWrapper classes={'info__item'} label={'Add name'} name={'name'}>
                                <Field name={'name'} className={'info__input'} type={'text'} />
                                {touched.name && errors.name && <span className={'input__error'}>{errors.name}</span>}
                            </InputWrapper>
                            <div className="info__unknown">
                                <InputWrapper
                                    classes={'info__item'}
                                    label={'Gender'}
                                    help={'Мужчина, Женщина, Неизвестен'}
                                    name={'gender'}
                                >
                                    <Field
                                        name={'gender.value'}
                                        className={'info__input info__input_short'}
                                        type={'text'}
                                        title={'Мужчина, Женщина, Неизвестен'}
                                    />
                                    {touched.gender && errors.gender && (
                                        <span className={'input__error'}>{errors.gender?.value}</span>
                                    )}
                                </InputWrapper>
                                <InputWrapper
                                    classes={'info__item'}
                                    label={'Race'}
                                    help={'Человек, Дроид, Вуки, Неизвестен'}
                                    name={'race'}
                                >
                                    <Field
                                        name={'race.value'}
                                        className={'info__input info__input_short'}
                                        type={'text'}
                                        title={'Человек, Дроид, Вуки, Неизвестен'}
                                    />
                                    {touched.race && errors.race && (
                                        <span className={'input__error'}>{errors.race?.value}</span>
                                    )}
                                </InputWrapper>
                                <InputWrapper
                                    classes={'info__item'}
                                    label={'Side'}
                                    help={'Светлая, Темная, Неизвестен'}
                                    name={'side'}
                                >
                                    <Field
                                        name={'side.value'}
                                        className={'info__input info__input_short'}
                                        type={'text'}
                                        title={'Светлая, Темная, Неизвестен'}
                                    />
                                    {touched.side && errors.side && (
                                        <span className={'input__error'}>{errors.side?.value}</span>
                                    )}
                                </InputWrapper>
                            </div>
                            <div className="info__item">
                                <div className="info__item-title">
                                    <label className="info__inline-label">Add description</label>
                                    <span className="info__span-counter">{`${values.description.length}/1000`}</span>
                                </div>
                                <Field
                                    name={'description'}
                                    className={'info__textarea'}
                                    as="textarea"
                                    maxLength={1000}
                                />
                            </div>
                            <div className="info__item">
                                <div className="info__item-title">
                                    <label className="info__inline-label">Add tag (use space to separate tags)</label>
                                    <span className="info__span-counter">{`${values.id.split(' ').length - 1}/3`}</span>
                                </div>
                                <Field
                                    name={'id'}
                                    value={values.id}
                                    className={'info__input'}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
                                        setFieldValue('id', event.target.value);

                                        if (values.id.split(' ').length <= MAX_TAGS_NUMBER) {
                                            setFieldValue('tag1', values.id.split(' ')[0]);
                                            setFieldValue('tag2', values.id.split(' ')[1]);
                                            setFieldValue('tag3', values.id.split(' ')[2]);
                                        }
                                    }}
                                />
                            </div>
                            <div className="info__footer">
                                <div className="info__photo">
                                    <div className="photo__container">
                                        {values.imageURL ? (
                                            <img src={values.imageURL} alt={values.imageURL}></img>
                                        ) : (
                                            <Field
                                                name={'imageURL'}
                                                className={'photo__addByUrl-input'}
                                                type={'input'}
                                                placeholder={'Upload by URL'}
                                            />
                                        )}
                                    </div>
                                </div>
                                <div className="info__color">
                                    <div className="color__item">
                                        <Field name={'nameColor'} className={'color__input'} type={'color'} />
                                        <span className="color__label">Name color</span>
                                    </div>
                                    <div className="color__item">
                                        <Field name={'backgroundColor'} className={'color__input'} type={'color'} />
                                        <span className="color__label">Background color of the parameters </span>
                                    </div>
                                    <div className="color__item">
                                        <Field name={'parametersColor'} className={'color__input'} type={'color'} />
                                        <span className="color__label">Color of parameters</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/************************** ADD CHARACTER PREVIEW **************************/}
                        <div className="modal__preview">
                            <div className="preview__close">
                                <span className="preview__close-text">Preview</span>
                                <ButtonClose img={close} closeHandler={props.closeModal} />
                            </div>
                            <div className="preview__views">
                                <span className={index === 0 ? 'preview__views-item active' : 'preview__views-item'}>
                                    View 1
                                </span>
                                <span className={index === 1 ? 'preview__views-item active' : 'preview__views-item'}>
                                    View 2
                                </span>
                                <div className="preview__slider">
                                    <div className="preview__slider-photo">
                                        <div
                                            className={
                                                index === 0 ? 'slider__photo photo-1 active' : 'slider__photo photo-1'
                                            }
                                        >
                                            <ViewModal
                                                closeModal={props.closeModal}
                                                viewedCharacter={viewedCharacterFactory(values)}
                                                fullSize={false}
                                            />
                                        </div>
                                        <div
                                            className={
                                                index === 1 ? 'slider__photo photo-2 active' : 'slider__photo photo-2'
                                            }
                                        >
                                            <Card card={viewedCharacterFactory(values)} />
                                        </div>
                                    </div>
                                    <Pagination
                                        currentIndex={index}
                                        totalDotsNumber={TOTAL_DOTS_NUMBER}
                                        getActualIndex={getActualIndex}
                                    />
                                </div>
                            </div>
                            <div className="preview__btn-container">
                                <button className="preview__btn" type="submit" disabled={!isValid && !dirty}>
                                    SAVE
                                </button>
                            </div>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    );
};

export default AddModal;
