export interface ICharacter {
    id: string;
    name: string;
    gender: string;
    race: string;
    side: string;
    image: string;
    descriptions?: string;
    tags: string[];
    nameColor?: string;
    parametersBgColor?: string;
    parametersColor?: string;
}
