import { FC } from 'react';

import Button from '../../components/button/Button';

export interface ButtonInterface {
    url: string;
    text: string;
    classes: string[];
}

const ErrorPage: FC = () => {
    return (
        <main className="main_error">
            <div className="main__content_error">
                <h1 className="main__title_error">This page is not fully armed and operational.</h1>
                <Button url="/" text="Return" classes={['main__btn', 'btn']} />
            </div>
        </main>
    );
};

export default ErrorPage;
