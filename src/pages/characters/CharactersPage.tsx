import qs from 'qs';
import { FC, useEffect } from 'react';
import { useHistory, useParams } from 'react-router';

import addBtn from '../../assets/image/addBtn.png';
import searchIcon from '../../assets/image/searchBtn.png';
import Input from '../../components/input/Input';

import Modal from '../../components/modal/Modal';
import Registry from '../../components/registry/Registry';
import Selectors from '../../components/select/Selectors';
import AddModal from '../../core/components/addModal/AddModal';
import ViewModal from '../../core/components/viewModal/ViewModal';

import {
    setAddCharacterModalToggle,
    setCharacterData,
    setFilterData,
    setGenderFilters,
    setRaceFilters,
    setSideFilters,
    setViewCharacterModalToggle,
} from '../../redux/character/characterPage.slice';
import { useAppDispatch, useAppSelector } from '../../redux/store.hooks';
import { charactersPageUrl, characterUrl } from '../../services/characterServices/character.constants';
import { getCard, getData } from '../../services/characterServices/character.service';
import { getGender, getRace, getSide } from '../../services/dictionaryServices/dictionary.service';

const CharactersPage: FC = (): React.ReactElement => {
    const { id: queryID } = useParams<{ id: string }>();
    const characterPage = useAppSelector((state) => state.characterPageState);
    const dispatch = useAppDispatch();
    const history = useHistory();

    useEffect(() => {
        const getFilterTypes = async (): Promise<void> => {
            if (location.search) {
                dispatch(
                    setFilterData({
                        ...characterPage.filterData,
                        ...qs.parse(location.search.slice(1)),
                    })
                );
            } else if (location.pathname === `${charactersPageUrl}/${queryID}`) {
                dispatch(setViewCharacterModalToggle({ isOpen: true, viewedCharacter: await getCard(queryID) }));
                history.push(`${charactersPageUrl}/${queryID}`);
            } else if (location.pathname === `${charactersPageUrl}/new`) {
                dispatch(setAddCharacterModalToggle(true));
                history.push(`${charactersPageUrl}/new`);
            }

            dispatch(setGenderFilters(await getGender()));
            dispatch(setRaceFilters(await getRace()));
            dispatch(setSideFilters(await getSide()));
        };

        void getFilterTypes();
    }, []);

    useEffect(() => {
        const getFilteredData = async (): Promise<void> => {
            if (
                characterPage.filterData.values !== '' ||
                characterPage.filterData.gender.length !== 0 ||
                characterPage.filterData.race.length !== 0 ||
                characterPage.filterData.side.length !== 0
            ) {
                history.push(`${charactersPageUrl}?${qs.stringify(characterPage.filterData)}`);
                dispatch(setCharacterData(await getData(`${characterUrl}?${qs.stringify(characterPage.filterData)}`)));
            } else {
                history.replace(charactersPageUrl);
                dispatch(setCharacterData(await getData(`${characterUrl}?${qs.stringify(characterPage.filterData)}`)));
            }
        };

        void getFilteredData();
    }, [characterPage.filterData]);

    const closeModal = (): void => {
        dispatch(setAddCharacterModalToggle(false));
        dispatch(setViewCharacterModalToggle({ isOpen: false, viewedCharacter: null }));
        history.replace(charactersPageUrl);
    };

    const searchOnChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        dispatch(
            setFilterData({
                ...characterPage.filterData,
                values: event.target.value,
            })
        );
    };

    return (
        <main className="main_characters">
            <div className="main__content_characters">
                <div className="main__title_characters">
                    <h1>Who's Your Favorite Star Wars</h1>
                </div>
                <div className="main__body_characters">
                    <div className="main__search">
                        <Input
                            classes={['main__search-input']}
                            type={'text'}
                            placeholder={'Search heroes'}
                            onChange={searchOnChange}
                            name={'search'}
                        />
                        <img className="main__search-icon" src={searchIcon} alt="Search button"></img>
                    </div>
                    <div className="main__filters">
                        <Selectors />
                        <div className="main__add">
                            <button
                                className="main__add-btn"
                                onClick={(): void => {
                                    dispatch(setAddCharacterModalToggle(true));
                                    history.replace(`${charactersPageUrl}/new`);
                                }}
                            >
                                Add
                                <img className="main__add-icon" src={addBtn} alt="Add btn"></img>
                            </button>
                        </div>
                    </div>
                    <Registry />
                </div>
            </div>
            {characterPage.addCharacterModalToggle ? (
                <Modal modalType={'addCharacter'} closeModal={closeModal} currentModal={<AddModal closeModal={closeModal} />} />
            ) : characterPage.viewCharacterModalToggle ? (
                <Modal
                    modalType={'viewCharacter'}
                    closeModal={closeModal}
                    currentModal={<ViewModal closeModal={closeModal} viewedCharacter={characterPage.viewedCharacter} fullSize={true} />}
                />
            ) : null}
        </main>
    );
};

export default CharactersPage;
