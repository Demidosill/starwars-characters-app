import { FC } from 'react';

import Button from '../../components/button/Button';

const HomePage: FC = () => {
    return (
        <main className="main_home">
            <div className="main-content_home">
                <h1 className="main__title_home">Find all your favorite heroes “Star Wars”</h1>
                <p className="main__subtitle_home">You can know the type of heroes, its strengths, disadvantages and abilities</p>
                <Button url="/preview" text="Start" classes={['main__btn', 'btn', 'btn_animation']} />
            </div>
        </main>
    );
};

export default HomePage;
